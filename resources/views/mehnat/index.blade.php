<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="{{ asset('public/apostille/img/favicon.ico') }}">
    <title>e-register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{{ asset('public/apostille/css/app.c816d106.css') }}" rel="preload" as="style">
    <link href="{{ asset('public/apostille/css/chunk-vendors.0014bfcb.css') }}" rel="preload" as="style">
    <link href="{{ asset('public/apostille/js/app.9837f053.js') }}" rel="preload" as="script">
    <link href="{{ asset('public/apostille/js/chunk-vendors.66f10add.js') }}" rel="preload" as="script">
    <link href="{{ asset('public/apostille/css/chunk-vendors.0014bfcb.css') }}" rel="stylesheet">
    <link href="{{ asset('public/apostille/css/app.c816d106.css') }}" rel="stylesheet">

    <!-- <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/main2.css"> -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>



{{--    <script--}}
{{--        type="text/javascript"--}}
{{--        async=""--}}
{{--        src="https://www.gstatic.com/recaptcha/releases/4PnKmGB9wRHh1i04o7YUICeI/recaptcha__en.js"--}}
{{--        crossorigin="anonymous"--}}
{{--        integrity="sha384-yVUHtIpds8Tl/RiQE0yJa9zbdbWLiDChDOh21M+FbehU63XhxluEyLXxS9zMIE4r"--}}
{{--    ></script>--}}
{{--    <script id="recaptcha-script" src="https://www.google.com/recaptcha/api.js?onload=recaptchaReady&amp;render=explicit&amp;hl=en"></script>--}}

</head>
<body>
<noscript>
    <strong>We're sorry but e-register-client doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
</noscript>
<div id="app" data-v-app="">
    <header class="app-header">
        <div class="container--max-width">
            <img class="app-header__logo" src="{{ asset('public/apostille/img/gerb.cb42fc79.png') }}" alt="gerb">
            <h2 class="app_header__title">Information system of electronic apostille and electronic register of certified documents</h2>
            <div class="lng-switcher app_header__lang-swr">
                <button class="lng-switcher__selected-lng">
                    <i class="icon-planet"></i>
                    <span class="lng-switcher__selected-lng-label">English</span>
                </button>
                <ul class="lng-switcher__langs-list" style="display: none;">
                    <li class="lng-switcher__langs-list-item">English</li>
                    <li class="lng-switcher__langs-list-item">Uzbek</li>
                </ul>
            </div>
        </div>
    </header>
    <main class="app-main">
        <div class="container--max-width">
            <h2 class="app-main__title">Verification of affixed apostilles in the Republic of Uzbekistan</h2>
            <section class="c-card" style="margin-bottom: 1rem;">
                <div class="c-card__header">
                    <h4 class="c-card__title">For verification, please enter the data of the existing Apostille</h4>
                    <h6 class="msg msg--warning">
                        <i class="msg__icon icon-warning"></i>
                        <span>All fields are required</span>
                    </h6>
                </div>
                <div class="c-card__body">

                    <form action="{{ route('get.client') }}" method="get">

                        <div class="search-form">
                            <div class="field-wrapp" required="">
                                <div class="field-wrapp__label">Apostille</div>
                                <input type="text" name="number" id="raqam"
                                       value="{{ request()->get('number') }}"
                                       autocomplete="off" class="f-input" placeholder="Indicate the Apostille number">
                                <div class="field-wrapp__error" style="display: none;"></div>
                            </div>


                            <div class="field-wrapp" required="">
                                <div class="field-wrapp__label">Date of Apostille affixing</div>
                                <div class="el-input el-input--prefix el-input--suffix el-date-editor el-date-editor--date f-dp">
                                    <!-- 前置元素 -->
                                    <!--v-if-->
                                    <input
                                        id="sana"
                                        class="el-input__inner px-5"
                                        name="date"
                                        aria-describedby="el-popper-67"
                                        type="text"
                                        autocomplete="off"
                                        placeholder="Specify the date of affixing"
                                        value="{{ request()->get('date') }}"
                                    >
                                    <!-- 前置内容 -->
                                    <span class="el-input__prefix ms-2">
                                            <i class="far fa-calendar-alt fa-xl" aria-hidden="true"></i>
                                        <!--v-if-->
                                        </span>
                                    <!-- 后置内容 -->
                                    <span class="el-input__suffix">
                                            <span class="el-input__suffix-inner">
                                                <i class="el-input__icon"></i>
                                                <!--v-if-->
                                                <!--v-if-->
                                                <!--v-if-->
                                                <!--v-if-->
                                            </span>
                                        <!--v-if-->
                                        </span>
                                    <!-- 后置元素 -->
                                    <!--v-if-->
                                    <!--v-if-->
                                </div>
                                <div class="field-wrapp__error" style="display: none;"></div>
                            </div>

                            <div class="search-form__separator"></div>
                            <div class="recaptcha ">
                                <div class="g-recaptcha"
                                     data-sitekey="6LdBe7ElAAAAAD1y6iEQzkwhoTQ-jn-EIQN9vNFC"
                                ></div>
                            </div>
                            <i class="search-form__dummy-field"></i>
                            <i class="search-form__dummy-field"></i>
                            <i class="search-form__dummy-field"></i>
                            <div class="search-form__btns">
                                <button class="f-button f-button--secondary" onclick="cleard()">Clear</button>
                                <button class="f-button f-button--primary {{ $client?'f-button--disabled':'' }}" type="submit">Find</button>
                            </div>
                        </div>


                    </form>

                    <script>

                        function cleard(){

                            let number = document.getElementById('raqam')
                            let date = document.getElementById('sana')

                            number.value = ''
                            date.value = ''

                        }

                    </script>

                </div>
            </section>
            <section class="c-card" style="margin-bottom: 1rem;">
                <div class="c-card__header">
                    <h4 class="c-card__title">About apostille</h4>
                </div>

                @if($client)

                    <div>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn d-block w-100 text-start mt-3" data-bs-toggle="modal" data-bs-target="#exampleModal">
                            <div class="doc-card">
                                <i class="icon-file"></i>
                                <div class="doc-card__label">Apostille No {{ $client->doc_number }} from {{ $client->doc_date }}</div>
                            </div>
                        </button>

                        <!-- Modal -->
                        <div class="modal fade " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-fullscreen w-90 mx-auto">
                                <div class="modal-content mt-3" style="border-radius: 10px">
                                    <!-- <div class="modal-header"> -->
                                    <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
                                    <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                                    <!-- </div> -->
                                    <!-- <div class="modal-body"> -->


{{--                                    <div class="dlg d-flex justify-content-center" style="display: block;">--}}
                                        <div class="dlg__content" style="display: block;">
                                            <div class="dlg__content-head">
                                                <span class="dlg__title"></span>
                                                <button class="dlg__btn-close icon-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="document-viewer">
                                                <!---->
                                                {{--                                            <iframe frameborder="0" src="https://apostille.davxizmat.uz/api/loadFile?entity=app_apostille&amp;attribute=file&amp;ID=3000033554241&amp;ct=application/pdf&amp;fName=Apostille_No_017443_from_2023-03-24.pdf" type="application/pdf"></iframe>--}}
                                                <iframe frameborder="0" src="{{ asset('public/storage/file/').'/'.$client->file }}" type="application/pdf"></iframe>
                                            </div>
                                        </div>
{{--                                    </div>--}}



                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="c-card__body mt-0">
                        <!---->

                        <div class="">
                            <h6 class="msg msg--info" style="margin: 20px 0px 10px;">
                                <i class="msg__icon icon-warning"></i>
                                <span>To view the document to be apostilled, enter its serial or registration number!</span>
                            </h6>
                            <div class="search-document-form">
                                <div class="field-wrapp" required="">
                                    <div class="field-wrapp__label">Document number</div>
                                    <input type="text" class="f-input" placeholder="Indicate the document number">
                                    <div class="field-wrapp__error" style="display: none;"></div>
                                </div>
                                <button class="f-button f-button--primary f-button" style="margin-bottom: 20px;">Look</button>
                            </div>
                        </div>
                        <!---->
                    </div>

                @else

                    <div class="c-card__body mt-1">
                        <h6 class="msg msg--primary"><i class="msg__icon icon-warning"></i><span>No results were found for the entered data</span></h6>
                    </div>

                @endif



            </section>
            <section class="c-card" style="margin-bottom: 1rem;">
                <div class="c-card__header">
                    <h4 class="c-card__title">About site</h4>
                </div>
                <div class="c-card__body">
                    <p class="about-us__p">On this website you can verify the authenticity of the Apostille in the Republic of Uzbekistan. You can also visually check the scanned versions of the Apostille documents.</p>
                    <p class="about-us__p">To verify the issued Apostille, you must enter the number and date of the Apostille document. When you click the Search button, the Apostille document and the main document will open. To view a document with an Apostille, you must enter the registration or serial number of the document in the appropriate field.</p>
                    <h6 class="about-us__h">Reminder</h6>
                    <p class="about-us__p">The application for the Apostille of documents by the state agency is made through the portal
                        <a href="https://my.gov.uz" target="_blank">my.gov.uz</a> - the Single interactive state services portal.
                    </p>
                </div>
            </section>
            <section class="c-card" style="margin-bottom: 1rem;">
                <div class="c-card__header">
                    <h4 class="c-card__title">Contacts</h4>
                </div>
                <div class="c-card__body">
                    <div>
                        <div class="contacts-card__title">General Prosecutor's Office of the Republic of Uzbekistan</div>
                        <ul class="contacts-card__list">
                            <li class="contacts-card__list-item">
                                <i class="contacts-card__list-item-icon icon-planet"></i>
                                <a class="contacts-card__list-item-value--link" href="https://prokuratura.uz" target="_blank">https://prokuratura.uz</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <div class="contacts-card__title">Supreme Court of the Republic of Uzbekistan</div>
                        <ul class="contacts-card__list">
                            <li class="contacts-card__list-item">
                                <i class="contacts-card__list-item-icon icon-planet"></i>
                                <a class="contacts-card__list-item-value--link" href="https://sud.uz" target="_blank">https://sud.uz</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <div class="contacts-card__title">Ministry of Justice of the Republic of Uzbekistan</div>
                        <ul class="contacts-card__list">
                            <li class="contacts-card__list-item">
                                <i class="contacts-card__list-item-icon icon-planet"></i>
                                <a class="contacts-card__list-item-value--link" href="https://minjust.uz" target="_blank">https://minjust.uz</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <div class="contacts-card__title">The Ministry of Foreign Affairs of the Republic of Uzbekistan</div>
                        <ul class="contacts-card__list">
                            <li class="contacts-card__list-item">
                                <i class="contacts-card__list-item-icon icon-planet"></i>
                                <a class="contacts-card__list-item-value--link" href="https://www.mfa.uz" target="_blank">https://www.mfa.uz</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <div class="contacts-card__title">The State Inspection for Supervision of Quality in Education under the Cabinet of Ministers the Republic of Uzbekistan</div>
                        <ul class="contacts-card__list">
                            <li class="contacts-card__list-item">
                                <i class="contacts-card__list-item-icon icon-planet"></i>
                                <a class="contacts-card__list-item-value--link" href="https://www.tdi.uz" target="_blank">https://www.tdi.uz</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </main>
    <footer class="app-footer">
        <div class="container--max-width">
            <span class="app-footer__dev-info">Developed with the support of the Joint Project of the Public Services Agency under the Ministry of Justice of the Republic of Uzbekistan, the European Union and UNDP in Uzbekistan "Improving the delivery of public services and improving the level of governance in rural areas of Uzbekistan.</span>
            <img src="{{ asset('public/apostille/img/eu_logo.d1762d58.svg') }}" class="app-footer__logo" alt="eu_logo">
            <img src="{{ asset('public/apostille/img/Minjust_logo.bc9e22e2.svg') }}" class="app-footer__logo" alt="Minjust_logo">
            <img src="{{ asset('public/apostille/img/PSA_logo.19b63c13.svg') }}" class="app-footer__logo" alt="PSA_logo">
            <img src="{{ asset('public/apostille/img/UNDP_logo.20a3ce99.svg') }}" class="app-footer__logo" alt="UNDP_logo">
        </div>
    </footer>
    <div class="preloader" style="display: none;">
        <div class="preloader__spinner">
            <svg class="preloader__spinner-circular" viewBox="25 25 50 50">
                <circle
                    class="preloader__spinner-path"
                    cx="50"
                    cy="50"
                    r="20"
                    fill="none"
                ></circle>
            </svg>
        </div>
    </div>
    <div class="toast-notifications"></div>
</div>
<!-- <script src="/js/chunk-vendors.66f10add.js"></script>
<script src="/js/app.9837f053.js"></script> -->
<div
    aria-hidden="true"
    class="el-picker__popper el-popper is-light is-pure"
    id="el-popper-67"
    role="tooltip"
    style="z-index: 2002; display: none;"
>
    <div class="el-picker-panel el-date-picker" actualvisible="false" unlinkpanels="false">
        <div class="el-picker-panel__body-wrapper">
            <!--v-if-->
            <div class="el-picker-panel__body">
                <!--v-if-->
                <div class="el-date-picker__header">
                    <button type="button" aria-label="Previous Year" class="el-picker-panel__icon-btn el-date-picker__prev-btn el-icon-d-arrow-left"></button>
                    <button type="button" aria-label="Previous Month" class="el-picker-panel__icon-btn el-date-picker__prev-btn el-icon-arrow-left"></button>
                    <span role="button" class="el-date-picker__header-label">2023</span>
                    <span role="button" class="el-date-picker__header-label">March</span>
                    <button type="button" aria-label="Next Year" class="el-picker-panel__icon-btn el-date-picker__next-btn el-icon-d-arrow-right"></button>
                    <button type="button" aria-label="Next Month" class="el-picker-panel__icon-btn el-date-picker__next-btn el-icon-arrow-right"></button>
                </div>
                <div class="el-picker-panel__content">
                    <table cellspacing="0" cellpadding="0" class="el-date-table">
                        <tbody>
                        <tr>
                            <!--v-if-->
                            <th>Sun</th>
                            <th>Mon</th>
                            <th>Tue</th>
                            <th>Wed</th>
                            <th>Thu</th>
                            <th>Fri</th>
                            <th>Sat</th>
                        </tr>
                        <tr class="el-date-table__row">
                            <td class="prev-month">
                                <div>
                                    <span>26</span>
                                </div>
                            </td>
                            <td class="prev-month">
                                <div>
                                    <span>27</span>
                                </div>
                            </td>
                            <td class="prev-month">
                                <div>
                                    <span>28</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>1</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>2</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>3</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>4</span>
                                </div>
                            </td>
                        </tr>
                        <tr class="el-date-table__row">
                            <td class="available">
                                <div>
                                    <span>5</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>6</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>7</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>8</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>9</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>10</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>11</span>
                                </div>
                            </td>
                        </tr>
                        <tr class="el-date-table__row">
                            <td class="available">
                                <div>
                                    <span>12</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>13</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>14</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>15</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>16</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>17</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>18</span>
                                </div>
                            </td>
                        </tr>
                        <tr class="el-date-table__row">
                            <td class="available">
                                <div>
                                    <span>19</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>20</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>21</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>22</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>23</span>
                                </div>
                            </td>
                            <td class="available current">
                                <div>
                                    <span>24</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>25</span>
                                </div>
                            </td>
                        </tr>
                        <tr class="el-date-table__row">
                            <td class="available">
                                <div>
                                    <span>26</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>27</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>28</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>29</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>30</span>
                                </div>
                            </td>
                            <td class="available">
                                <div>
                                    <span>31</span>
                                </div>
                            </td>
                            <td class="next-month">
                                <div>
                                    <span>1</span>
                                </div>
                            </td>
                        </tr>
                        <tr class="el-date-table__row">
                            <td class="next-month">
                                <div>
                                    <span>2</span>
                                </div>
                            </td>
                            <td class="next-month">
                                <div>
                                    <span>3</span>
                                </div>
                            </td>
                            <td class="next-month">
                                <div>
                                    <span>4</span>
                                </div>
                            </td>
                            <td class="next-month">
                                <div>
                                    <span>5</span>
                                </div>
                            </td>
                            <td class="next-month">
                                <div>
                                    <span>6</span>
                                </div>
                            </td>
                            <td class="next-month">
                                <div>
                                    <span>7</span>
                                </div>
                            </td>
                            <td class="next-month">
                                <div>
                                    <span>8</span>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!--v-if-->
                    <!--v-if-->
                </div>
            </div>
        </div>
        <div class="el-picker-panel__footer" style="display: none;">
            <button class="el-button el-button--text el-button--mini el-picker-panel__link-btn" type="button">
                <!--v-if-->
                <!--v-if-->
                <span>Now</span>
            </button>
            <button class="el-button el-button--default el-button--mini is-plain el-picker-panel__link-btn" type="button">
                <!--v-if-->
                <!--v-if-->
                <span>OK</span>
            </button>
        </div>
    </div>
    <div class="el-popper__arrow" data-popper-arrow="" style=""></div>
</div>



{{--<div class="dlg" style="display: none;">--}}
{{--    <div class="dlg__content" style="display: none;">--}}
{{--        <div class="dlg__content-head">--}}
{{--            <span class="dlg__title"></span>--}}
{{--            <button class="dlg__btn-close icon-close"></button>--}}
{{--        </div>--}}
{{--        <div class="document-viewer">--}}
{{--            <!---->--}}
{{--            <iframe frameborder="0" src="https://apostille.davxizmat.uz/api/loadFile?entity=app_apostille&amp;attribute=file&amp;ID=3000033554241&amp;ct=application/pdf&amp;fName=Apostille_No_017443_from_2023-03-24.pdf" type="application/pdf"></iframe>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}



{{--<div style="background-color: rgb(255, 255, 255); border: 1px solid rgb(204, 204, 204); box-shadow: rgba(0, 0, 0, 0.2) 2px 2px 3px; position: absolute; transition: visibility 0s linear 0.3s, opacity 0.3s linear 0s; opacity: 0; visibility: hidden; z-index: 2000000000; left: 0px; top: -10000px;">--}}
{{--    <div style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 2000000000; background-color: rgb(255, 255, 255); opacity: 0.05;"></div>--}}
{{--    <div class="g-recaptcha-bubble-arrow" style="border: 11px solid transparent; width: 0px; height: 0px; position: absolute; pointer-events: none; margin-top: -11px; z-index: 2000000000;"></div>--}}
{{--    <div class="g-recaptcha-bubble-arrow" style="border: 10px solid transparent; width: 0px; height: 0px; position: absolute; pointer-events: none; margin-top: -10px; z-index: 2000000000;"></div>--}}
{{--    <div style="z-index: 2000000000; position: relative; width: 0px; height: 0px;">--}}
{{--        <iframe--}}
{{--            title="recaptcha challenge expires in two minutes"--}}
{{--            src="https://www.google.com/recaptcha/api2/bframe?hl=en&amp;v=4PnKmGB9wRHh1i04o7YUICeI&amp;k=6Lfu068ZAAAAAIkX9Tqt_zBrY0L37TFVndqIl7Zp"--}}
{{--            name="c-yn9u7nmpswa"--}}
{{--            frameborder="0"--}}
{{--            scrolling="no"--}}
{{--            sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"--}}
{{--            style="width: 0px; height: 0px;"--}}
{{--        ></iframe>--}}
{{--    </div>--}}
{{--</div>--}}



<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
</html>
