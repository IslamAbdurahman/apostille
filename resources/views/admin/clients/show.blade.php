@extends('layouts.main')
@section('main-content')

    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <ul class="navbar list-unstyled m-0 p-0">
                <li>
                    <h3 class="card-title"><b>{{ $client->name }}</b> <span>{{ $client->passport }}</span> MEHNAT FAOLIYATI</h3>
                </li>
                <li>

                    <button type="button" class="btn btn-success my-0 py-0 px-1 " data-toggle="modal" data-target="#create">
                        <i class="fa fa-user-plus"></i>
                    </button>
                    <div class="modal fade" id="create">
                        <div class="modal-dialog">
                            <div class="modal-content bg-primary">
                                <form action="{{ route('mehnat.store') }}" method="post">

                                    <div class="modal-header">
                                        <h4 class="modal-title">Klient qo'shish</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body py-0">

                                        @csrf
                                        <div class="card-body py-0">


                                            <div class="form-group">
                                                <label for="exampleInputFile">Sana</label>
                                                <div class="input-group">
                                                    <input type="text" name="sana" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputFile">Lavozim</label>
                                                <div class="input-group">
                                                    <input type="text" name="lavozim" class="form-control" required placeholder="Lavozim kiriting">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputFile">Bo‘lim</label>
                                                <div class="input-group">
                                                    <input type="text" name="bolim" class="form-control" required placeholder="Bo‘lim kiriting">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputFile">Tashkilot</label>
                                                <div class="input-group">
                                                    <input type="text" name="tashkilot" class="form-control" required placeholder="Tashkilot kiriting">
                                                </div>
                                            </div>

                                            <input type="hidden" name="client_id" value="{{ $id }}">


                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                    </div>

                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </li>
            </ul>
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0 table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th class="py-1">N</th>
                    <th class="py-1">Sana</th>
                    <th class="py-1">Lavozim</th>
                    <th class="py-1">Bo‘lim</th>
                    <th class="py-1">Tashkilot</th>
                    <th class="py-1">Action

                    </th>
                </tr>
                </thead>


                <tbody>
                @foreach($mehnat as $item)
                    <tr>
                        <td class="py-1">
                            {{$loop->index+1}}
                        </td>
                        <td class="py-1">{{ $item->sana }}</td>
                        <td class="py-1">{{ $item->lavozim }}</td>
                        <td class="py-1">{{ $item->bolim }}</td>
                        <td class="py-1">{{ $item->tashkilot }}</td>
                        <td class="py-1">

                            <button type="button" class="btn btn-danger my-0 py-0 px-1" data-toggle="modal" data-target="#modal-danger{{ $item->id }}">
                                <i class="fas fa-trash m-0 p-0"></i>
                            </button>

                            <div class="modal fade" id="modal-danger{{ $item->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-danger">
                                        <div class="modal-header">
                                            <h4 class="modal-title">O'chirish</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>O'chirishni xohlaysizmi?</p>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                            <form class="d-inline" action="{{ route('mehnat.destroy',$item->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-outline-light">
                                                    O'chirish
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </td>
                    </tr>
                @endforeach
                </tbody>


            </table>

        </div>

        <div class="card">
            <div class="card-header">
                <ul class="navbar list-unstyled m-0 p-0">
                    <li>
                        <h3 class="card-title"><b>{{ $client->name }}</b> <span>{{ $client->passport }}</span> O'zini o'zi band qilish</h3>
                    </li>
                    <li>

                        <button type="button" class="btn btn-success my-0 py-0 px-1 " data-toggle="modal" data-target="#create_band">
                            <i class="fa fa-user-plus"></i>
                        </button>
                        <div class="modal fade" id="create_band">
                            <div class="modal-dialog">
                                <div class="modal-content bg-primary">
                                    <form action="{{ route('band_qilish.store') }}" method="post" enctype="multipart/form-data">

                                        <div class="modal-header">
                                            <h4 class="modal-title">Klient qo'shish</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body py-0">

                                            @csrf
                                            <div class="card-body py-0">


                                                <div class="form-group">
                                                    <label for="exampleInputFile">Faoliyat boshlagan sana</label>
                                                    <div class="input-group">
                                                        <input type="text" name="bosh_sana" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputFile">Faoliyat turi</label>
                                                    <div class="input-group">
                                                        <input type="text" name="faoliyat" class="form-control" required placeholder="Faoliyat turi kiriting">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputFile">Faoliyatni amalga oshirish manzili</label>
                                                    <div class="input-group">
                                                        <input type="text" name="address" class="form-control" required placeholder="Manzil kiriting">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInputFile">Document</label>
                                                    <div class="input-group">
                                                        <input type="file" name="doc" class="form-control"  placeholder="File kiriting">
                                                    </div>
                                                </div>

                                                <input type="hidden" name="client_id" value="{{ $id }}">


                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                    <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                                </div>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                        </div>

                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </li>
                </ul>
            </div>
            <!-- /.card-header -->
            <div class="card-body pt-0 table-responsive">
                <table id="example1" class="table table-bordered table-striped ">
                    <thead>
                    <tr>
                        <th class="py-1">N</th>
                        <th class="py-1">Faoliyat boshlagan sana</th>
                        <th class="py-1">Faoliyat turi</th>
                        <th class="py-1">Faoliyatni amalga oshirish manzili</th>
                        <th class="py-1">Document</th>
                        <th class="py-1">Action</th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach($band_qilish as $item)
                        <tr>
                            <td class="py-1">
                                {{$loop->index+1}}
                            </td>
                            <td class="py-1">{{ $item->bosh_sana }}</td>
                            <td class="py-1">{{ $item->faoliyat }}</td>
                            <td class="py-1">{{ $item->address }}</td>
                            <td class="py-1">
                                <a href="{{ asset('public/storage/selfemployment/get-document/'.$item->doc) }}" download>DOCUMENT</a>
                            </td>
                            <td class="py-1">

                                <button type="button" class="btn btn-danger my-0 py-0 px-1" data-toggle="modal" data-target="#modal-danger_band{{ $item->id }}">
                                    <i class="fas fa-trash m-0 p-0"></i>
                                </button>

                                <div class="modal fade" id="modal-danger_band{{ $item->id }}">
                                    <div class="modal-dialog">
                                        <div class="modal-content bg-danger">
                                            <div class="modal-header">
                                                <h4 class="modal-title">O'chirish</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>O'chirishni xohlaysizmi?</p>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                                <form class="d-inline" action="{{ route('band_qilish.destroy',$item->id) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn btn-outline-light">
                                                        O'chirish
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->

                            </td>
                        </tr>
                    @endforeach
                    </tbody>


                </table>

            </div>
@endsection
