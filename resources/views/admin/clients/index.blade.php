@extends('layouts.main')
@section('main-content')

    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <ul class="navbar list-unstyled m-0 p-0">
                <li>
                    <h3 class="card-title">Klientlar</h3>
                </li>
                <li>

                    <button type="button" class="btn btn-success my-0 py-0 px-1 " data-toggle="modal" data-target="#create">
                        <i class="fa fa-user-plus"></i>
                    </button>
                    <div class="modal fade" id="create">
                        <div class="modal-dialog">
                            <div class="modal-content bg-primary">
                                <form action="{{ route('client.store') }}" method="post" enctype="multipart/form-data">

                                    <div class="modal-header">
                                        <h4 class="modal-title">Klient qo'shish</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body py-0">

                                        @csrf
                                        <div class="card-body py-0">

                                            <div class="form-group">
                                                <label for="exampleInputFile">File</label>
                                                <div class="input-group">
                                                    <input type="file" name="file" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputFile">Xujjat raqami</label>
                                                <div class="input-group">
                                                    <input type="number" name="doc_number" class="form-control" required placeholder="Xujjat raqami kiriting">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputFile">Berilgan sana</label>
                                                <div class="input-group">
                                                    <input type="date" name="doc_date" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputFile">Fuqaro</label>
                                                <div class="input-group">
                                                    <input type="text" name="name" class="form-control" required placeholder="Fuqaro kiriting">
                                                </div>
                                            </div>

{{--                                            <div class="form-group">--}}
{{--                                                <label for="exampleInputFile">Passport raqami</label>--}}
{{--                                                <div class="input-group">--}}
{{--                                                    <input type="text" name="passport" class="form-control" required placeholder="Passport raqami kiriting">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                            <div class="form-group">--}}
{{--                                                <label for="exampleInputFile">JSHSHIR raqami</label>--}}
{{--                                                <div class="input-group">--}}
{{--                                                    <input type="number" min="10000000000000" max="99999999999999"--}}
{{--                                                           name="jshshir" class="form-control" required placeholder="JSHSHIR raqami kiriting">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                            <div class="form-group">--}}
{{--                                                <label for="exampleInputFile">STIR raqami</label>--}}
{{--                                                <div class="input-group">--}}
{{--                                                    <input type="number" min="1000000000" max="9999999999"--}}
{{--                                                           name="stir" class="form-control" placeholder="STIR raqami kiriting">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                            <div class="form-group">--}}
{{--                                                <label for="exampleInputFile">Tug'ilgan sana</label>--}}
{{--                                                <div class="input-group">--}}
{{--                                                    <input type="text" name="birth_date" class="form-control">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                            <div class="form-group">--}}
{{--                                                <label for="exampleInputFile">Yashash joyi</label>--}}
{{--                                                <div class="input-group">--}}
{{--                                                    <input type="text" name="address" class="form-control" required placeholder="Yashash joyi kiriting">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}



                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                    </div>

                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </li>
                <li>
                    <!-- SEARCH FORM -->
                    <form action="{{ route('client.index') }}" method="get" class="form-inline m-0 ml-md-3">
                        @csrf
                        <div class="input-group input-group-sm">
                            <select name="per_page" id="" class="form-control form-control-navbar">
                                <option {{ $per_page == 10 ? 'selected':'' }} value="10">10</option>
                                <option {{ $per_page == 20 ? 'selected':'' }}  value="20">20</option>
                                <option {{ $per_page == 50 ? 'selected':'' }}  value="50">50</option>
                                <option {{ $per_page == 100 ? 'selected':'' }}  value="100">100</option>
                            </select>
                            <input name="search" value="{{ $search }}" class="form-control form-control-navbar" type="search" placeholder="Qidirish" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0 table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th class="py-1">N</th>
                    <th class="py-1">Fuqaro</th>
                    <th class="py-1">File</th>
                    <th class="py-1">Xujjat raqami</th>
                    <th class="py-1">Berilgan sana</th>
                    <th class="py-1">Link</th>
{{--                    <th class="py-1">Паспорт рақами</th>--}}
{{--                    <th class="py-1">JSHSHIR</th>--}}
{{--                    <th class="py-1">STIR</th>--}}
{{--                    <th class="py-1">Tug'ilgan sana</th>--}}
{{--                    <th class="py-1">Yashash joyi</th>--}}
                    <th class="py-1">Action

                    </th>
                </tr>
                </thead>


                <tbody>
                @foreach($clients as $client)
                    <tr>
                        <td class="py-1">
                            {{($clients->currentpage()-1)*$clients->perpage()+($loop->index+1)}}
                        </td>
                        <td class="py-1">
{{--                            <a href="{{ route('client.show', $client->id) }}">--}}
                                {{ $client->name }}
{{--                            </a>--}}
                        </td>
                        <td class="py-1">
                            <a href="{{ asset('public/storage/file/'.$client->file) }}" download>
                                {{ $client->file }}
                            </a>
                        </td>
                        <td class="py-1">{{ $client->doc_number }}</td>
                        <td class="py-1">{{ $client->doc_date }}</td>

                        <td class="py-1">
                            https://apostille.dav.xizmati.uz/?number={{ $client->doc_number }}&date={{ $client->doc_date }}
                        </td>
{{--                        <td class="py-1">{{ $client->passport }}</td>--}}
{{--                        <td class="py-1">{{ $client->jshshir }}</td>--}}
{{--                        <td class="py-1">{{ $client->stir }}</td>--}}
{{--                        <td class="py-1">{{ $client->birth_date }}</td>--}}
{{--                        <td class="py-1">{{ $client->address }}</td>--}}
                        <td class="py-1">

                            <button type="button" class="btn btn-success my-0 py-0 px-1 " data-toggle="modal" data-target="#update{{$loop->index}}">
                                <i class="fas fa-user-edit"></i>
                            </button>
                            <div class="modal fade" id="update{{$loop->index}}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-primary">
                                        <form action="{{ route('client.update',$client->id) }}" method="post" enctype="multipart/form-data">

                                            @method('put')
                                            <div class="modal-header">
                                                <h4 class="modal-title">Klient Tahrirlash</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body py-0">

                                                @csrf
                                                <div class="card-body py-0">

                                                    <div class="form-group">
                                                        <label for="exampleInputFile">File</label>
                                                        <div class="input-group">
                                                            <input type="file" name="file" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputFile">Xujjat raqami</label>
                                                            <div class="input-group">
                                                                <input name="doc_number" type="number" value="{{ $client->doc_number }}" class="form-control" required placeholder="Xujjat raqami kiriting">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputFile">Berilgan sana</label>
                                                        <div class="input-group">
                                                            <input type="date"  value="{{ $client->doc_date }}"  name="doc_date" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputFile">Fuqaro</label>
                                                        <div class="input-group">
                                                                <input type="text"  value="{{ $client->name }}"  name="name" class="form-control" required placeholder="Fuqaro kiriting">
                                                        </div>
                                                    </div>

{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="exampleInputFile">Passport raqami</label>--}}
{{--                                                        <div class="input-group">--}}
{{--                                                            <input type="text"  value="{{ $client->passport }}"  name="passport" class="form-control" required placeholder="Passport raqami kiriting">--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}

{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="exampleInputFile">JSHSHIR raqami</label>--}}
{{--                                                        <div class="input-group">--}}
{{--                                                            <input type="number"  value="{{ $client->jshshir }}"  min="10000000000000" max="99999999999999"--}}
{{--                                                                   name="jshshir" class="form-control" required placeholder="JSHSHIR raqami kiriting">--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}

{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="exampleInputFile">STIR raqami</label>--}}
{{--                                                        <div class="input-group">--}}
{{--                                                            <input type="number"  value="{{ $client->stir }}"  min="1000000000" max="9999999999"--}}
{{--                                                                   name="stir" class="form-control" placeholder="STIR raqami kiriting">--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}

{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="exampleInputFile">Tug'ilgan sana</label>--}}
{{--                                                        <div class="input-group">--}}
{{--                                                            <input type="text"  value="{{ $client->birth_date }}"  name="birth_date" class="form-control">--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}

{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="exampleInputFile">Yashash joyi</label>--}}
{{--                                                        <div class="input-group">--}}
{{--                                                            <input type="text"  value="{{ $client->address }}"  name="address" class="form-control" required placeholder="Yashash joyi kiriting">--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}



                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                        <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                            </div>

                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                            <button type="button" class="btn btn-danger my-0 py-0 px-1" data-toggle="modal" data-target="#modal-danger{{ $client->id }}">
                                <i class="fas fa-trash m-0 p-0"></i>
                            </button>

                            <div class="modal fade" id="modal-danger{{ $client->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-danger">
                                        <div class="modal-header">
                                            <h4 class="modal-title">O'chirish</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>O'chirishni xohlaysizmi?</p>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
                                            <form class="d-inline" action="{{ route('client.destroy',$client->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-outline-light">
                                                    O'chirish
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                        </td>
                    </tr>
                @endforeach
                </tbody>


            </table>

            <div>{{ $clients->appends($_GET)->links() }}</div>
        </div>
@endsection
