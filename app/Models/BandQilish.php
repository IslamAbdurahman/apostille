<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BandQilish extends Model
{
    use HasFactory;
    protected $table = 'band_qilish';
    public $timestamps = false;

    protected $fillable = [
        'bosh_sana',
        'faoliyat',
        'address',
        'doc',
        'client_id'
    ];
}
