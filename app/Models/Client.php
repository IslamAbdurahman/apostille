<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $fillable = [
        'doc_number',
        'doc_date',
        'name',
        'passport',
        'jshshir',
        'stir',
        'birth_date',
        'address',
        'image',
        'file',
    ];

    public $timestamps = false;
}
