<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mehnat extends Model
{
    use HasFactory;

    protected $table = 'mehnat';
    public $timestamps = false;

    protected $fillable = [
        'sana',
        'lavozim',
        'bolim',
        'tashkilot',
        'client_id'
    ];
}
