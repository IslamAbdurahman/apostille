<?php

namespace App\Http\Controllers;

use App\Models\BandQilish;
use App\Models\Client;
use App\Models\Mehnat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->per_page && $request->search){
            $clients = DB::table('clients as c')
            ->select('c.*')
            ->where('c.name','like','%'.$request->search.'%')
            ->paginate($request->per_page);

            $per_page = $request->per_page;
            $search = $request->search;
        }elseif ($request->per_page){
            $clients = DB::table('clients as c')
                ->select('c.*')
                ->where('c.name','like','%'.$request->search.'%')
                ->paginate($request->per_page);

            $per_page = $request->per_page;
            $search = '';
        }else{
            $clients = Client::paginate(10);
            $per_page = 10;
            $search = '';
        }

        return view('admin.clients.index',compact('clients','per_page','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'doc_number'=>'required',
            'doc_date'=>'required',
            'name'=>'required',
//            'passport'=>'required',
//            'jshshir'=>'required',
//            'birth_date'=>'required',
//            'address'=>'required',
        ]);

        if ($request->hasFile('file')) {
            $filename = time() . rand(1000, 9999) . "." . $request->file->extension();

            $filename = 'Apostille_No_'.$request->doc_number.'_from_'.$request->doc_date.".".$request->file->extension();

            $request->file('file')->storeAs('public/file', $filename);
        }else{
            $filename = 'no_file';
        }

            $client = Client::create([
                'doc_number'=>$request->doc_number,
                'doc_date'=>$request->doc_date,
                'name'=>$request->name,
//                'passport'=>$request->passport,
//                'jshshir'=>$request->jshshir,
//                'stir'=>$request->stir,
//                'birth_date'=>$request->birth_date,
//                'address'=>$request->address,
                'file'=>$filename,
            ]);



        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $mehnat = Mehnat::where('client_id','=',$id)->get();
        $band_qilish = BandQilish::where('client_id','=',$id)->get();
        $client = Client::find($id);


        return view('admin.clients.show',compact('mehnat','band_qilish','id','client'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'doc_number'=>'required',
            'doc_date'=>'required',
            'name'=>'required',
//            'passport'=>'required',
//            'jshshir'=>'required',
//            'birth_date'=>'required',
//            'address'=>'required',
        ]);

//        dd($request,$request->hasFile('file'));



        $client = Client::find($id);
        $client->doc_number = $request->doc_number;
        $client->doc_date = $request->doc_date;
        $client->name = $request->name;

        if ($request->hasFile('file')) {
            $filename = time() . rand(1000, 9999) . "." . $request->file->extension();

            $filename = 'Apostille_No_'.$request->doc_number.'_from_'.$request->doc_date.".".$request->file->extension();

            $request->file('file')->storeAs('public/file', $filename);
        }else{
            $filename = $client->file;
        }

//        $client->passport = $request->passport;
//        $client->jshshir = $request->jshshir;
//        $client->stir = $request->stir;
//        $client->birth_date = $request->birth_date;
//        $client->address = $request->address;
        $client->file = $filename;

        $client->update();


        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        if (File::exists(storage_path("app/public/file/".$client->file))){
            File::delete(storage_path("app/public/file/".$client->file));
        }
        $client->delete();

        return redirect()->back();
    }
}
