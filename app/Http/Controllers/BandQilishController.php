<?php

namespace App\Http\Controllers;

use App\Models\BandQilish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BandQilishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bosh_sana'=>'required',
            'faoliyat'=>'required',
            'address'=>'required',
            'client_id'=>'required'
        ]);

        if ($request->hasFile('doc')) {
            $filename = time() . rand(1000, 9999) . "." . $request->doc->extension();
            $request->file('doc')->storeAs('public/selfemployment/get-document', $filename);
        }else{
            $filename = 'undefined';
        }

        BandQilish::create([
            'bosh_sana'=>$request->bosh_sana,
            'faoliyat'=>$request->faoliyat,
            'doc'=>$filename,
            'address'=>$request->address,
            'client_id'=>$request->client_id
        ]);

        return redirect()->route('client.show',$request->client_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $band = BandQilish::find($id);
        if (File::exists(storage_path("app/public/selfemployment/get-document/".$band->doc))){
            File::delete(storage_path("app/public/selfemployment/get-document/".$band->doc));
        }
        $c_id = $band->client_id;
        $band->delete();

        return redirect()->route('client.show',$c_id);
    }
}
