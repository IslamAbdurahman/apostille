<?php

namespace App\Http\Controllers;

use App\Models\Mehnat;
use Illuminate\Http\Request;

class MehnatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'sana'=>'required',
            'lavozim'=>'required',
            'bolim'=>'required',
            'tashkilot'=>'required',
            'client_id'=>'required'
        ]);

        Mehnat::create([
            'sana'=>$request->sana,
            'lavozim'=>$request->lavozim,
            'bolim'=>$request->bolim,
            'tashkilot'=>$request->tashkilot,
            'client_id'=>$request->client_id
        ]);

        return redirect()->route('client.show',$request->client_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mehnat = Mehnat::find($id);
        $c_id = $mehnat->client_id;
        $mehnat->delete();

        return redirect()->route('client.show',$c_id);

    }
}
