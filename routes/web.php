<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return redirect()->to('https://my.gov.uz/');
////    return view('welcome');
////    return redirect()->route('login');
//});

Route::middleware(['auth'])->group(function () {

    Route::get('index', function (){
        return view('index');
    })->name('home.index');

    ///// Super admin routes /////
    Route::group(['prefix'=>'admin','middleware'=>'super_admin'], function (){

        Route::resources([
            'admin'=>\App\Http\Controllers\AdminController::class]);

    });

    ///// Admin routes /////
    Route::group(['prefix'=>'admin','middleware'=>'admin'], function (){

        Route::resource('admin', \App\Http\Controllers\AdminController::class)->only([
            'index', 'show','edit','update'
        ]);

    });

    ///// Manager routes /////
    Route::group(['prefix'=>'admin','middleware'=>'manager'], function (){

        Route::resource('admin', \App\Http\Controllers\AdminController::class)->only([
            'show','edit','index'
        ]);

        Route::resource('buyer', \App\Http\Controllers\BuyerController::class)->only([
            'index'
        ]);
    });

    Route::resource('client',\App\Http\Controllers\ClientController::class);
    Route::resource('mehnat',\App\Http\Controllers\MehnatController::class);
    Route::resource('band_qilish',\App\Http\Controllers\BandQilishController::class);


});


Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('',function (Request $request){



    if ($request->number && $request->date && $request['g-recaptcha-response']){
        $client = \App\Models\Client::where('doc_number','=',$request->number)
            ->where('doc_date','=',$request->date)
            ->first();

    }else{
        $client = false;
    }

//    dd($client);


    return view('mehnat.index',compact('client'));
})->name('get.client');


//Route::get('check',function (){
//
//    return url('https://my.mehnat.uz');
//});
//
//Route::post('find-client',function (Request $request){
//
//    return redirect()->route('get.client',$request->doc_number);
//
//})->name('find.client');
